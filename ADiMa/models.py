from django.db import models

ETATSTICKET = (
			(0, 'En attente de reception'),
			(1, 'Recu'),
			(2, 'En cours'),
			(3, 'En attente'),
			(4, 'Termine, en attente d\'envoi'),
			(5, 'Termine, revoye'),
			)

class Pays (models.Model) :
	nom_pays = models.CharField(max_length=16) #
	
	def get_all_ids_and_names(self):
		countries = self.all()
		countries_list = list()
		for country in countries:
			countries_list.append((country.id, country.nom_pays))
		
		return countries_list
	
	def __str__(self):
		return self.nom_pays

class Contact (models.Model) :
	societe = models.CharField(max_length=128, blank=True) # varchar(128)
	nom = models.CharField(max_length=32) # varchar(32)
	prenom = models.CharField(max_length=32, blank=True) # varchar(32)
	adresse = models.CharField(max_length=32) # varchar(32)
	adresse2 = models.CharField(max_length=32, blank=True) # varchar(32)
	ville = models.CharField(max_length=32) # varchar(32)
	codepostal = models.CharField(max_length=5) # varchar(5)
	pays = models.ForeignKey(Pays) # 
	tel = models.CharField(max_length=10, blank=True) # varchar(10)
	indic_tel = models.CharField(max_length=4, blank=True) # varchar(4)
	email = models.CharField(max_length=64, blank=True) # varchar(32)
	
	def fill_fields(self, fields_array):
		if fields_array['id'] != '':
			self.id = int(fields_array['id'])
		self.nom = fields_array['nom']
		self.prenom = fields_array['prenom']
		self.societe = fields_array['societe']
		self.adresse = fields_array['adresse']
		self.adresse2 = fields_array['adresse2']
		self.ville = fields_array['ville']
		self.codepostal = fields_array['codepostal']
		self.tel = fields_array['tel']
		self.indic_tel = fields_array['indic_tel']
		self.email = fields_array['email']
		self.pays = Pays.objects.get(pk=int(fields_array['pays']))
	
	def get_form_init(self):
		return {'id':self.id,
                'nom':self.nom,
                'prenom':self.prenom,
                'societe':self.societe,
                'adresse':self.adresse,
                'adresse2':self.adresse2,
                'ville':self.ville,
                'codepostal':self.codepostal,
                'pays':self.pays,
                'email':self.email,
                'tel':self.tel,
                'indic_tel':self.indic_tel,}
	
	def is_client(self):
		try:
			Client.objects.get(contact=self)
		except Client.DoesNotExist:
			return False
		return True
	
	def is_reseller(self):
		try:
			Revendeur.objects.get(contact=self)
		except Revendeur.DoesNotExist:
			return False
		return True
	
	def is_prospect(self):
		try:
			Prospect.objects.get(contact=self)
		except Prospect.DoesNotExist:
			return False
		return True
	
	def __str__(self):
		if(self.societe != ""):
			return self.societe + " - " + self.nom + " " + self.prenom
		else:
			return self.nom + " " + self.prenom

class Client (models.Model) :
	contact = models.ForeignKey(Contact) # 
	
	def __str__(self):
		return self.contact.nom + " " + self.contact.prenom

class Revendeur (models.Model) :
	contact = models.ForeignKey(Contact) #
	
	def __str__(self):
		return self.contact.nom + " " + self.contact.prenom

class Transporteur (models.Model) :
	societe = models.CharField(max_length=128) # varchar(128)
	nom = models.CharField(max_length=32, blank = True) # varchar(32)
	prenom = models.CharField(max_length=32, blank=True) # varchar(32)
	adresse = models.CharField(max_length=32, blank=True) # varchar(32)
	adresse2 = models.CharField(max_length=32, blank=True) # varchar(32)
	ville = models.CharField(max_length=32, blank=True) # varchar(32)
	codepostal = models.CharField(max_length=5, blank=True) # varchar(5)
	pays = models.ForeignKey(Pays) # 
	tel = models.CharField(max_length=10, blank=True) # varchar(10)
	indic_tel = models.CharField(max_length=4, blank=True) # varchar(4)
	email = models.CharField(max_length=64, blank=True) # varchar(32)
	
	def __str__(self):
		return self.societe
	
class Prospect (models.Model) :
	contact = models.ForeignKey(Contact) # 
	
	def __str__(self):
		return self.contact
	

class MarqueBatterie (models.Model) :
	nom_marque = models.CharField(max_length=64) # 
	contact = models.ForeignKey(Contact, blank=True, null=True) # 
	
	def get_products(self):
		return ModeleBatterie.objects.all().filter(marque_bat=self)
	
	def __str__(self):
		return self.nom_marque

class MarqueVelo (models.Model) :
	nom_marque = models.CharField(max_length=32) # varchar(32)
	contact = models.ForeignKey(Contact) # int
	
	def get_products(self):
		return ModeleVelo.objects.all().filter(marque_velo=self)
	
	def __str__(self):
		return self.nom_marque

class MarquePieceDetachee (models.Model) :
	nom_marque = models.CharField(max_length=32) # varchar(32)
	contact = models.ForeignKey(Contact) # int
	
	def get_products(self):
		return PieceDetachee.objects.all().filter(marque=self)
	
	def __str__(self):
		return self.nom_marque

class PieceDetachee (models.Model) :
	nom = models.CharField(max_length=32) # varchar(32)
	description = models.CharField(max_length=128, blank=True) # varchar(128)
	contact_fournisseur = models.ForeignKey(Contact) # int
	prix = models.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)
	marque = models.ForeignKey(MarquePieceDetachee, blank=True, null=True)
	
	def __str__(self):
		return self.nom

class FacturationVente (models.Model) :
	contact = models.ForeignKey(Contact) # 
	total = models.DecimalField(max_digits=8, decimal_places=2) # 
	date = models.DateField() # date
	
	def __str__(self):
		return self.contact + " : " + self.total

class ModeleVelo (models.Model) :
	nom = models.CharField(max_length=16) # varchar(16)
	marque_velo = models.ForeignKey(MarqueVelo) # int
	
	def get_form_init(self):
		return {'nom':self.nom,
				'marque_velo':int(self.marque_velo.id),}
	
	def __str__(self):
		return self.nom

class ModeleBatterie (models.Model) :
	nom = models.CharField(max_length=64) # 
	marque_bat = models.ForeignKey(MarqueBatterie) # 
	tension_nom = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	tension_min = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	tension_max = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	courant_nom = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	courant_min = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	courant_max = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True) # 
	capacite_nom = models.IntegerField(blank=True, null=True) # 
	temps_garantie_neuf = models.IntegerField(blank=True, null=True) # 
	temps_garantie_recond = models.IntegerField(blank=True, null=True) # 
	prix_neuf = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # 
	prix_recond = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # 
	
	def __str__(self):
		return self.nom

class Batterie (models.Model) :
	num_serie_batterie = models.CharField(max_length=32) # 
	modele_batterie = models.ForeignKey(ModeleBatterie) # 
	client = models.ForeignKey(Client, blank=True, null=True) # 
	revendeur = models.ForeignKey(Revendeur, blank=True, null=True) # 
	piecedet_chargeur = models.ForeignKey(PieceDetachee, blank=True, null=True) # 
	date_garantie = models.DateField() # 
	contact_proprietaire = models.ForeignKey(Contact) # 
	
	def __str__(self):
		return self.modele_batterie.nom + " " + self.num_serie_batterie

class VersionVelo (models.Model) :
	designation = models.CharField(max_length=16) # varchar(16)
	modele_velo = models.ForeignKey(ModeleVelo) # int
	modele_batterie = models.ForeignKey(ModeleBatterie) # int
	pieces = models.ManyToManyField(PieceDetachee, blank=True)
	temps_garantie_neuf = models.IntegerField(blank=True, null=True) # int
	temps_garantie_recond = models.IntegerField(blank=True, null=True) # int
	prix_neuf = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # numeric(8,2)
	prix_recond = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) # numeric(8,2)
	
	def get_form_init(self):
		return {'designation':self.designation,
				'modele_velo':int(self.modele_velo.id),
				}
	
	def __str__(self):
		return self.designation + "(" + self.modele_velo.nom + ", " + self.modele_batterie.nom + ")"

class MouvementInventaireVelo (models.Model) :
	rel_object = models.ForeignKey(VersionVelo) # int
	entree = models.BooleanField(default=False) # boolean
	quantite = models.IntegerField() # int
	date = models.DateTimeField() # date
	desc = models.CharField(max_length=128, blank=True) # varchar(128)
	attrib = models.CharField(max_length=32, blank=True) # varchar(32)
	
	def __str__(self):
		if(self.entree):
			sens = "ENTREE"
		else:
			sens = "SORTIE"
		return str(self.date) + " - " + sens + " : x" + str(self.quantite) + " " + str(self.rel_object) + ((" " + self.attrib) if self.attrib!="" else "")

class MouvementInventaireBatterie (models.Model) :
	rel_object = models.ForeignKey(ModeleBatterie) # 
	entree = models.BooleanField(default=False) # boolean
	quantite = models.IntegerField() # int
	date = models.DateTimeField() # date
	desc = models.CharField(max_length=128, blank=True) # varchar(128)
	
	def __str__(self):
		if(self.entree):
			sens = "ENTREE"
		else:
			sens = "SORTIE"
		return str(self.date) + " - " + sens + " : x" + str(self.quantite) + " " + str(self.rel_object)

class MouvementInventairePieceDetachee (models.Model) :
	rel_object = models.ForeignKey(PieceDetachee) # int
	entree = models.BooleanField(default=False) # boolean
	quantite = models.IntegerField() # int
	date = models.DateTimeField() # date
	desc = models.CharField(max_length=128, blank=True) # varchar(128)
	
	def __str__(self):
		if(self.entree):
			sens = "ENTREE"
		else:
			sens = "SORTIE"
		return str(self.date) + " - " + sens + str(self.quantite) + " : " + str(self.rel_object)


class Velo (models.Model) :
	num_serie_cadre = models.CharField(max_length=32) # varchar(32)
	marque = models.ForeignKey(MarqueVelo) # int
	modele_velo = models.ForeignKey(ModeleVelo) # int
	version_velo = models.ForeignKey(VersionVelo) # int
	batterie = models.ForeignKey(Batterie) # int
	debut_garantie = models.DateField() # date
	contact_proprietaire = models.ForeignKey(Contact) # int
	
	def fill_fields(self, fields_array):
		self.num_serie_cadre = fields_array['num_serie_cadre']
		try:
			self.marque = MarqueVelo.objects.get(id=int(fields_array['marque']))
		except MarqueVelo.DoesNotExist:
			pass
		try:
			self.modele_velo = ModeleVelo.objects.get(id=int(fields_array['modele_velo']))
		except ModeleVelo.DoesNotExist:
			pass
		try:
			self.version_velo = VersionVelo.objects.get(id=int(fields_array['version_velo']))
		except VersionVelo.DoesNotExist:
			pass
		try:
			self.batterie = Batterie.objects.get(id=int(fields_array['batterie']))
		except Batterie.DoesNotExist:
			pass
		try:
			self.contact_proprietaire = MarqueVelo.objects.get(id=int(fields_array['contact_proprietaire']))
		except Contact.DoesNotExist:
			pass
		self.debut_garantie = fields_array['debut_garantie']
	
	def get_form_init(self):
		return {'num_serie_cadre': self.num_serie_cadre,
				'marque': int(self.marque.id),
				'modele_velo': int(self.modele_velo.id),
				'version_velo': int(self.version_velo.id),
				'batterie': int(self.batterie.id),
				'debut_garantie': self.debut_garantie,
				'contact_proprietaire': self.contact_proprietaire,
				}
	
	def __str__(self):
		return self.modele_velo.nom + " " + self.version_velo.designation + " " + self.contact_proprietaire.nom + " " + self.num_serie_cadre

class EnvoiLivraison (models.Model) :
	contact = models.ForeignKey(Contact) # 
	transporteur = models.ForeignKey(Transporteur) # int
	prix = models.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)
	date_envoi = models.DateField() # date
	date_reception = models.DateField(blank=True, null=True) # date
	id_suivi_transporteur = models.CharField(max_length=30, blank=True) # varchar(30)
	contenu_velos = models.ManyToManyField(Velo, blank=True)
	contenu_batteries = models.ManyToManyField(Batterie, blank=True)
	contenu_pieces = models.ManyToManyField(PieceDetachee, blank=True)
	
	def __str__(self):
		return str(self.contact) + " " + str(self.date_envoi)
	

class ReceptionLivraison (models.Model) :
	contact = models.ForeignKey(Contact) # 
	date_reception = models.DateField() # date
	contenu_velos = models.ManyToManyField(Velo, blank=True)
	contenu_batteries = models.ManyToManyField(Batterie, blank=True)
	contenu_pieces = models.ManyToManyField(PieceDetachee, blank=True)
	
	def __str__(self):
		return str(self.contact) + " " + str(self.date_reception)

class TicketSAV (models.Model) :
	envoi = models.ForeignKey(EnvoiLivraison, blank=True, null=True) # 
	reception = models.ForeignKey(ReceptionLivraison, blank=True, null=True) # 
	client = models.ForeignKey(Client, blank=True, null=True) # int
	revendeur = models.ForeignKey(Revendeur, blank=True, null=True) # int
	etat = models.IntegerField(choices=ETATSTICKET) # tinyint(2)
	urgence = models.BooleanField(default=False) # boolean
	clos = models.BooleanField(default=False) # boolean
	nombre_heures = models.IntegerField() # int
	debut = models.DateField(blank=True, null=True) # date
	fin = models.DateField(blank=True, null=True) # date
	contenu_velos = models.ManyToManyField(Velo, blank=True)
	contenu_batteries = models.ManyToManyField(Batterie, blank=True)
	contenu_pieces = models.ManyToManyField(PieceDetachee, blank=True)
	
	def get_ticket_owner(self):
		if(self.client!=None):
			return self.client.contact.nom
		elif(self.revendeur!=None):
			return self.revendeur.contact.nom
		else:
			return "Revendeur ou Client non specifie"
	
	def __str__(self):
		closed = "CLOS" if (self.clos) else "EN COURS"
		return closed + " | " + self.get_ticket_owner()

class FacturationSAV (models.Model) :
	ticket = models.ForeignKey(TicketSAV) # 
	detail = models.CharField(max_length=1024, blank=True) # varchar(1024)
	total = models.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)
	date = models.DateField() # date
	
	def __str__(self):
		return self.ticket.getContact() + " " + self.total

class OperationTicketSAV (models.Model) :
	ticket = models.ForeignKey(TicketSAV) # 
	description = models.CharField(max_length=255) # varchar(255)
	debut = models.DateField(blank=True, null=True) # date
	fin = models.DateField(blank=True, null=True) # date
	
	def is_over(self):
		if self.debut != None and self.fin !=None:
			return True
		else:
			return False
			
	def __str__(self):
		return self.ticket.get_ticket_owner() + " " + self.description + " (" + ("FINI" if self.is_over() else "EN COURS") + ")"
	
