from django import forms
from ADiMa.models import *
from django.forms.extras.widgets import SelectDateWidget

TICKETSTATES = (
            (0, 'En attente de reception'),
            (1, 'Recu'),
            (2, 'En cours'),
            (3, 'En attente'),
            (4, 'Termine, en attente d\'envoi'),
            (5, 'Termine, revoye'),
            )

INVENTORY_INOUT = (
                   (True, "Entree"),
                   (False, "Sortie"),
                   )

class ContactForm(forms.Form):
    id = forms.IntegerField(widget=forms.HiddenInput(), required=False)
    societe = forms.CharField(max_length=128, required=False) # varchar(128)
    nom = forms.CharField(max_length=32) # varchar(32)
    prenom = forms.CharField(max_length=32, required=False) # varchar(32)
    adresse = forms.CharField(max_length=32) # varchar(32)
    adresse2 = forms.CharField(max_length=32, required=False) # varchar(32)
    ville = forms.CharField(max_length=32) # varchar(32)
    codepostal = forms.CharField(max_length=5) # varchar(5)
    pays = forms.ModelChoiceField(queryset=Pays.objects, empty_label=None) 
    tel = forms.CharField(max_length=10, required=False) # varchar(10)
    indic_tel = forms.CharField(max_length=4, required=False) # varchar(4)
    email = forms.EmailField(max_length=64, required=False) # varchar(32)

class ContactAttributesForm(forms.Form):
    make_client = forms.BooleanField()
    make_reseller = forms.BooleanField()
    make_prospect = forms.BooleanField()    


class BrandForm(forms.Form):
    nom_marque = forms.CharField(max_length=64)
    contact = forms.ModelChoiceField(queryset=Contact.objects)


class BikeModelForm(forms.Form):
    nom = forms.CharField(max_length=16) # varchar(16)
    marque_velo = forms.ModelChoiceField(queryset=MarqueVelo.objects)


class BikeVersionForm(forms.Form):
    designation = forms.CharField(max_length=16) # varchar(16)
    modele_velo = forms.ModelChoiceField(queryset=ModeleVelo.objects) # int
    modele_batterie = forms.ModelChoiceField(queryset=ModeleBatterie.objects) # int
    pieces = forms.ModelMultipleChoiceField(queryset=PieceDetachee.objects)
    temps_garantie_neuf = forms.IntegerField() # int
    temps_garantie_recond = forms.IntegerField() # int
    prix_neuf = forms.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)
    prix_recond = forms.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)


class BikeForm(forms.Form):
    num_serie_cadre = forms.CharField(max_length=32) # varchar(32)
    marque = forms.ModelChoiceField(queryset=MarqueVelo.objects) # int
    modele_velo = forms.ModelChoiceField(queryset=ModeleVelo.objects) # int
    version_velo = forms.ModelChoiceField(queryset=VersionVelo.objects) # int
    batterie = forms.ModelChoiceField(queryset=Batterie.objects) # int
    debut_garantie = forms.DateField() # date
    contact_proprietaire = forms.ModelChoiceField(queryset=Contact.objects) # int


class BatteryModelForm(forms.Form):
    nom = forms.CharField(max_length=64) # 
    marque_bat = forms.ModelChoiceField(queryset=MarqueBatterie.objects) # 
    tension_nom = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    tension_min = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    tension_max = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    courant_nom = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    courant_min = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    courant_max = forms.DecimalField(max_digits=4, decimal_places=2, required=False) # 
    capacite_nom = forms.IntegerField(required=False) # 
    prix = forms.DecimalField(max_digits=8, decimal_places=2, required=False) # 
    temps_garantie_neuf = forms.IntegerField() # 
    temps_garantie_recond = forms.IntegerField() # 
    prix_neuf = forms.DecimalField(max_digits=8, decimal_places=2) # 
    prix_recond = forms.DecimalField(max_digits=8, decimal_places=2) #


class BatteryForm(forms.Form):
    num_serie_batterie = forms.CharField(max_length=32) # 
    modele_batterie = forms.ModelChoiceField(queryset=ModeleBatterie.objects) # 
    client = forms.ModelChoiceField(queryset=Client.objects, required=False) # 
    revendeur = forms.ModelChoiceField(queryset=Revendeur.objects, required=False) # 
    piecedet_chargeur = forms.ModelChoiceField(queryset=PieceDetachee.objects, required=False) # 
    date_garantie = forms.DateField() # 
    contact_proprietaire = forms.ModelChoiceField(queryset=Contact.objects) #
    
class PartsModelForm (forms.Form) :
    nom = forms.CharField(max_length=32) # varchar(32)
    description = forms.CharField(max_length=128) # varchar(128)
    contact_fournisseur = forms.ModelChoiceField(queryset=Contact.objects) # int
    prix = forms.DecimalField(max_digits=8, decimal_places=2) # numeric(8,2)
    marque = forms.ModelChoiceField(queryset=MarquePieceDetachee.objects)


class TicketForm(forms.Form):
    envoi = forms.ModelChoiceField(queryset=EnvoiLivraison.objects, required=False) # 
    reception = forms.ModelChoiceField(queryset=ReceptionLivraison.objects, required=False) # 
    client = forms.ModelChoiceField(queryset=Client.objects, required=False) # int
    revendeur = forms.ModelChoiceField(queryset=Revendeur.objects, required=False) # int
    etat = forms.ChoiceField(choices=TICKETSTATES) # tinyint(2)
    urgence = forms.BooleanField(initial=False) # boolean
    clos = forms.BooleanField(initial=False) # boolean
    nombre_heures = forms.IntegerField() # int
    debut = forms.DateField(required=False) # date
    fin = forms.DateField(required=False) # date

class InventoryForm(forms.Form):
    entree = forms.ChoiceField(choices=INVENTORY_INOUT, widget=forms.RadioSelect)
    quantite = forms.IntegerField()