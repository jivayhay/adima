'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_parts_models(request, list_start=0, list_end=50):
    models_list = PieceDetachee.objects.all()[int(list_start):int(list_end)]
    return render(request, "list_models_parts.html", {'bikes_list': models_list,
                                                      'type': "pièce détachée",
                                                      'link_show': "show parts model",
                                                      'link_create': "create parts model",
                                                      })

def show_parts_model(request, product_id):
    if request.method == 'POST':
        model_form = PartsModelForm(request.POST)
        if model_form.is_valid():
            try:
                model = PieceDetachee.objects.get(id=request.product_id)
                model.nom = request.POST['nom']
                model.marque_bat = int(request.POST['marque_bat'])
                model.save()
            except PieceDetachee.DoesNotExist:
                raise Http404
        else:
            return HttpResponse("invalid")
    else:
        try:
            model = PieceDetachee.objects.get(id=product_id)
        except PieceDetachee.DoesNotExist:
            return Http404
        
    model_form = PartsModelForm(initial=model.get_form_init())
    return render(request, 'show_model_parts.html', {'model' : model,
                                                    'model_form' : model_form,
                                                    })

def create_parts_model(request):
    if request.method == 'POST':
        form = PartsModelForm(request.POST)
        if form.is_valid():
            model = PieceDetachee()
            model.nom = request.POST['nom']
            try:
                model.marque_bat = MarqueVelo.objects.get(id=int(request.POST['marque_bat']))
            except MarqueVelo.DoesNotExist:
                raise Http404
            model.save()
            return redirect('show parts model', product_id = model.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BikeModelForm()
    
    return render(request, 'show_model_parts_create.html', {'model_form' : form,})