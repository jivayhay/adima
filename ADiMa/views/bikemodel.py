'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_bike_models(request, list_start=0, list_end=50):
    models_list = ModeleVelo.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_models_bike.html', {'models_list': models_list,
                                                     'type': "vélo",
                                                     'link_show': "show bike model",
                                                     'link_create': "create bike model",})

def show_bike_model(request, product_id):
    if request.method == 'POST':
        model_form = BikeModelForm(request.POST)
        if model_form.is_valid():
            try:
                model = ModeleVelo.objects.get(id=request.product_id)
                model.nom = request.POST['nom']
                model.marque_velo = int(request.POST['marque_velo'])
                model.save()
            except ModeleVelo.DoesNotExist:
                raise Http404
        else:
            return HttpResponse("invalid")
    else:
        try:
            model = ModeleVelo.objects.get(id=product_id)
            related_versions = VersionVelo.objects.all().filter(modele_velo=model)
        except ModeleVelo.DoesNotExist:
            return Http404
        
    model_form = BikeModelForm(initial=model.get_form_init())
    return render(request, 'show_model_bike_base.html', {'model' : model,
                                                    'versions' : related_versions,
                                                    'model_form' : model_form,})

def create_bike_model(request):
    if request.method == 'POST':
        form = BikeModelForm(request.POST)
        if form.is_valid():
            model = ModeleVelo()
            model.nom = request.POST['nom']
            try:
                model.marque_velo = MarqueVelo.objects.get(id=int(request.POST['marque_velo']))
            except MarqueVelo.DoesNotExist:
                raise Http404
            model.save()
            return redirect('show bike model', product_id = model.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BikeModelForm()
    
    return render(request, 'show_model_bike_create.html', {'model_form' : form,})