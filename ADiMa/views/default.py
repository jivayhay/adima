'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render

def index(request):
    return render(request, 'base.html', None)