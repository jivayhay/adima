'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_contacts(request, list_start=0, list_end=25):
    contacts_list = Contact.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_contacts_base.html', {'contacts_list' : contacts_list})


def show_contact(request, contact_id):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            try:
                contact = Contact.objects.get(pk=request.POST['id'])
            except Contact.DoesNotExist:
                raise Http404
            
            contact.fill_fields(request.POST)            
            contact.save()
            
        else:
            return HttpResponse("erreur")
    else:
        try:
            contact = Contact.objects.get(pk=contact_id)
        except Contact.DoesNotExist:
            raise Http404
        
    form = ContactForm(initial=contact.get_form_init())
    return render(request, 'show_contact_base.html', {'contact' : contact, 
                                                     'contactform' : form,
                                                     'is_client' : contact.is_client(),
                                                     'is_reseller' : contact.is_reseller(),
                                                     'is_prospect' : contact.is_prospect(),})


def create_contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact = Contact()
            contact.fill_fields(request.POST)
            contact.save()
            
            if request.POST.get('make_client', False):
                client = Client()
                client.contact = contact
                client.save()
            if request.POST.get('make_reseller', False):
                reseller = Revendeur()
                reseller.contact = contact
                reseller.save()
            if request.POST.get('make_prospect', False):
                prospect = Prospect()
                prospect.contact = contact
                prospect.save()
            return redirect('show contact', contact_id=contact.id)
        else:
            return HttpResponse("erreur")
    else:     
        form = ContactForm()
        attributesform = ContactAttributesForm()
        return render(request, 'show_contact_create.html', {'contactform' : form,
                                                            'attributesform' : attributesform,
                                                            })