'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_prospects(request, list_start=0, list_end=25):
    prospects_list = Prospect.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_clientsresellersprospects_base.html', {'entry_list' : prospects_list,
                                                                        'type' : "prospect",
                                                                        'link_show' : "show prospect",
                                                                        'link_create' : "create prospect",})


def show_prospect_by_contact_id(request, contact_id):
    try:
        prospect = Prospect.objects.get(contact=Contact.objects.get(id=contact_id))
    except (Prospect.DoesNotExist, Contact.DoesNotExist):
        return Http404
    
    return redirect('show prospect', prospect_id=prospect.id)

def show_prospect(request, prospect_id):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid(): #and client_id == request.POST['id']:
            try:
                prospect = Prospect.objects.get(id=prospect_id)
            except Prospect.DoesNotExist:
                return Http404
            
            prospect.contact.fill_fields(request.POST)
            prospect.contact.save()
        
        else:
            return HttpResponse("erreur")
    else:
        try:
            prospect = Prospect.objects.get(id=prospect_id)
        except Client.DoesNotExist:
            return Http404
    
    form = ContactForm(initial=prospect.contact.get_form_init())
    return render(request, 'show_contact_prospect.html', {'prospect' : prospect, 
                                                     'contactform' : form,})

def create_prospect(request, contact_id=None):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            prospect = Prospect()
            contact = Contact()
            contact.fill_fields(request.POST)
            contact.save()
            prospect.contact = contact
            prospect.save()
            return redirect('show prospect', prospect_id=prospect.id)
        
    prospect = Prospect()
    if(contact_id != None):
        try:
            prospect.contact = Contact.objects.get(id=int(contact_id))
            form = ContactForm(initial=prospect.contact.get_form_init())
        except Contact.DoesNotExist:
            return Http404
    else:
        form = ContactForm()
        
    return render(request, 'show_contact_prospect_create.html', {'prospect' : prospect, 
                                                               'contactform' : form,})