'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_clients(request, list_start=0, list_end=25):
    clients_list = Client.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_clientsresellersprospects_base.html', {'entry_list' : clients_list,
                                                                        'type' : "client",
                                                                        'link_show' : "show client",
                                                                        'link_create' : "create client",})


def show_client_by_contact_id(request, contact_id):
    try:
        client = Client.objects.get(contact=Contact.objects.get(id=contact_id))
    except (Client.DoesNotExist, Contact.DoesNotExist):
        return Http404
    
    return redirect('show client', client_id=client.id)

def show_client(request, client_id):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid(): #and client_id == request.POST['id']:
            try:
                client = Client.objects.get(id=client_id)
            except Client.DoesNotExist:
                return Http404
            
            client.contact.fill_fields(request.POST)
            client.contact.save()
        
        else:
            return HttpResponse("erreur")
        
    else:
        try:
            client = Client.objects.get(id=client_id)
        except Client.DoesNotExist:
            return Http404
        
    form = ContactForm(initial=client.contact.get_form_init())
    return render(request, 'show_contact_client.html', {'client' : client, 
                                                         'contactform' : form,})


def create_client(request, contact_id=None):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            client = Client()
            contact = Contact()
            contact.fill_fields(request.POST)
            contact.save()
            client.contact = contact
            client.save()
            return redirect('show client', client_id=client.id)
        
    client = Client()
    if(contact_id != None):
        try:
            client.contact = Contact.objects.get(id=int(contact_id))
            form = ContactForm(initial=client.contact.get_form_init())
        except Contact.DoesNotExist:
            return Http404
    else:
        form = ContactForm()
        
    return render(request, 'show_contact_client_create.html', {'client' : client, 
                                                               'contactform' : form,})