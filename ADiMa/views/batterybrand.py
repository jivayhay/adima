'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_battery_brands(request, list_start=0, list_end=25):
    brands_list = MarqueBatterie.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_brands_base.html', {'brands_list' : brands_list,
                                                        'link_show' : "show battery brand",
                                                        'link_create' : "create battery brand",
                                                        })


def show_battery_brand(request, brand_id):
    if request.method == 'POST':
        form = BrandForm(request.POST)
        if form.is_valid():
            try:
                brand = MarqueBatterie.objects.get(pk=request.POST['id'])
            except Contact.DoesNotExist:
                raise Http404
            
            brand.fill_fields(request.POST)            
            brand.save()
            
        else:
            return HttpResponse("erreur")
    else:
        try:
            brand = MarqueBatterie.objects.get(id=brand_id)
        except MarqueBatterie.DoesNotExist:
            raise Http404
    
    products = brand.get_products()
    form = BrandForm(initial={'id':brand_id,
                                'nom_marque':brand.nom_marque,
                                'contact':brand.contact.id
                                })
    return render(request, 'show_brand_base.html', {'brand' : brand, 
                                                     'brandform' : form,
                                                     'products' : products,
                                                     'link_show_brand': "show battery brand",
                                                     'link_show_product': "show battery model",
                                                     })


def create_battery_brand(request):
    if request.method == 'POST':
        form = BrandForm(request.POST)
        if form.is_valid():
            brand = MarqueBatterie()
            brand.nom_marque = request.POST['nom_marque']
            brand.contact = Contact.objects.get(id=int(request.POST['contact']))
            brand.save()
            return redirect('show battery brand', brand_id = brand.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BrandForm()
    
    return render(request, 'show_brand_create.html', {'brandform' : form,
                                                      'link_create' : "create battery brand",
                                                      })