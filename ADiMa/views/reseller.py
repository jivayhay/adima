'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_resellers(request, list_start=0, list_end=25):
    resellers_list = Revendeur.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_clientsresellersprospects_base.html', {'entry_list' : resellers_list,
                                                                        'type' : "revendeur",
                                                                        'link_show' : "show reseller",
                                                                        'link_create' : "create reseller",})


def show_reseller_by_contact_id(request, contact_id):
    try:
        reseller = Revendeur.objects.get(contact=Contact.objects.get(id=contact_id))
    except (Revendeur.DoesNotExist, Contact.DoesNotExist):
        return Http404
    
    return redirect('show reseller', reseller_id=reseller.id)

def show_reseller(request, reseller_id):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid(): #and client_id == request.POST['id']:
            try:
                reseller = Revendeur.objects.get(id=reseller_id)
            except Revendeur.DoesNotExist:
                return Http404
            
            reseller.contact.fill_fields(request.POST)
            reseller.contact.save()
        
        else:
            return HttpResponse("erreur")
    else:
        try:
            reseller = Revendeur.objects.get(id=reseller_id)
        except Revendeur.DoesNotExist:
            return Http404
    
    form = ContactForm(initial=reseller.contact.get_form_init())
    return render(request, 'show_contact_reseller.html', {'reseller' : reseller, 
                                                     'contactform' : form,})

def create_reseller(request, contact_id=None):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            reseller = Revendeur()
            contact = Contact()
            contact.fill_fields(request.POST)
            contact.save()
            reseller.contact = contact
            reseller.save()
            return redirect('show reseller', reseller_id=reseller.id)
        
    reseller = Revendeur()
    if(contact_id != None):
        try:
            reseller.contact = Contact.objects.get(id=int(contact_id))
            form = ContactForm(initial=reseller.contact.get_form_init())
        except Contact.DoesNotExist:
            return Http404
    else:
        form = ContactForm()
        
    return render(request, 'show_contact_reseller_create.html', {'reseller' : reseller, 
                                                               'contactform' : form,})