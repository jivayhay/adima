'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_parts_brands(request, list_start=0, list_end=25):
    brands_list = MarquePieceDetachee.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_brands_base.html', {'brands_list' : brands_list,
                                                      'link_show' : "show parts brand",
                                                      'link_create' : "create parts brand",
                                                      })


def show_parts_brand(request, brand_id):
    if request.method == 'POST':
        form = BrandForm(request.POST)
        if form.is_valid():
            try:
                brand = MarquePieceDetachee.objects.get(pk=request.POST['id'])
            except Contact.DoesNotExist:
                raise Http404
            
            brand.fill_fields(request.POST)            
            brand.save()
            
        else:
            return HttpResponse("erreur")
    else:
        try:
            brand = MarquePieceDetachee.objects.get(pk=brand_id)
        except Contact.DoesNotExist:
            raise Http404

    products = brand.get_products()        
    form = ContactForm(initial={'id':brand_id,
                                'nom':brand.nom_marque,
                                })
    return render(request, 'show_brand_base.html', {'brand' : brand, 
                                                     'brandform' : form,
                                                     'products' : products,
                                                     'link_show_brand': "show parts brand",
                                                     'link_show_product': "show parts model",
                                                     })


def create_parts_brand(request):
    if request.method == 'POST':
        form = BrandForm(request.POST)
        if form.is_valid():
            brand = MarquePieceDetachee()
            brand.nom_marque = request.POST['nom_marque']
            brand.contact = Contact.objects.get(id=int(request.POST['contact']))
            brand.save()
            return redirect('show parts brand', brand_id = brand.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BrandForm()
    
    return render(request, 'show_brand_create.html', {'brandform' : form,
                                                      'link_create' : "create parts brand",
                                                      })