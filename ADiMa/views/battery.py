'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *


def list_batteries(request, list_start=0, list_end=50):
    batteries_list = Batterie.objects.all()[int(list_start):int(list_end)]
    return render(request, "list_batteries_base.html", {'batteries_list': batteries_list,})

def show_battery(request, product_id):
    if request.method == 'POST':
        battery_form = BatteryForm(request.POST)
        if battery_form.is_valid():
            try:
                battery = Batterie.objects.get(id=request.product_id)
            except Batterie.DoesNotExist:
                raise Http404
            battery.fill_fields(request.POST)
            battery.save()
            
        else:
            return HttpResponse("invalid")
    else:
        try:
            battery = Batterie.objects.get(id=product_id)
        except Batterie.DoesNotExist:
            return Http404
        
    battery_form = BatteryForm(initial=battery.get_form_init())
    return render(request, 'show_battery.html', {'battery' : battery,
                                              'battery_form': battery_form,
                                              })

def create_battery(request):
    if request.method == 'POST':
        form = BatteryForm(request.POST)
        if form.is_valid():
            battery = Batterie()
            battery.nom = request.POST['nom']
            return redirect('show battery', product_id = battery.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BikeModelForm()
    
    return render(request, 'show_battery_create.html', {'form' : form,})