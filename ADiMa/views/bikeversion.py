'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *

def list_bike_versions(request, list_start=0, list_end=50):
    versions = VersionVelo.objects.all()[int(list_start):int(list_end)]
    return render(request, 'list_versions_bike.html', {'versions_list': versions,})

def show_bike_version(request, product_id):
    try:
        version = VersionVelo.objects.get(id=product_id)
        related_products = Velo.objects.all().filter(version_velo=version)
    except ModeleVelo.DoesNotExist:
        return Http404
    
    bike_version_form = BikeVersionForm(initial=version.get_form_init())
    return render(request, 'show_version_bike.html', {'version' : version,
                                                    'products' : related_products,
                                                    'version_form' : bike_version_form})

def create_bike_version(request):
    if request.method == 'POST':
        form = BikeVersionForm(request.POST)
        if form.is_valid():
            version = VersionVelo()
            #TODO
            version.save()
            return redirect('show bike version', version_id = version.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BrandForm()
    
    return render(request, 'show_version_bike_create.html', {'version_form' : form,
                                                             })