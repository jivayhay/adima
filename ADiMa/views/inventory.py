'''
Created on 23 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *
import sys, time

def get_identifier(object_name):
    try:
        if object_name == "Velo":
            return getattr(sys.modules[__name__], "VersionVelo")
        elif object_name == "Batterie":
            return getattr(sys.modules[__name__], "ModeleBatterie")
        else:
            return getattr(sys.modules[__name__], object_name)
    except AttributeError:
        raise NameError("%s doesn't exist" %object_name)


def list_inventory(request, object_name):
    try:
        identifier = get_identifier(object_name)
        inventory_identifier = getattr(sys.modules[__name__], "MouvementInventaire" + object_name)
    except AttributeError:
        raise NameError("%s doesn't exist" %object_name)
    
    try:
        objects_list = identifier.objects.all()
        items_list = list()
        for item in objects_list:
            quantity = 0
            try:
                inventory = inventory_identifier.objects.all().filter(rel_object = item)
                for inventory_item in inventory:
                    if inventory_item.entree:
                        quantity += inventory_item.quantite
                    else:
                        quantity -= inventory_item.quantite
                
            except inventory_identifier.DoesNotExist:
                raise Http404
            items_list.append({'item': item, 'quantity': quantity})
            
    except identifier.DoesNotExist:
        raise Http404
    
    return render(request, 'inventory_base.html', {'object_name': object_name,
                                                   'items_list': items_list,
                                                   })

def show_inventory_detail(request, object_name, object_id):
    try:
        identifier = get_identifier(object_name)
        inventory_identifier = getattr(sys.modules[__name__], "MouvementInventaire" + object_name)
    except AttributeError:
        raise NameError("%s doesn't exist" %object_name)
    
    if request.method== 'POST':
        form = InventoryForm(request.POST)
        if form.is_valid():
            new_inventory_entry = inventory_identifier()
            new_inventory_entry.quantite = request.POST['quantite']
            new_inventory_entry.date = time.strftime("%Y-%m-%d")
            if request.POST['entree'] == 'True':
                new_inventory_entry.entree = True
            else:
                new_inventory_entry.entree = False
            
            try:
                new_inventory_entry.rel_object = identifier.objects.get(id=int(request.POST['item_id']))
            except identifier.DoesNotExist:
                raise Http404
            
            new_inventory_entry.save()
    
    try:
        item = identifier.objects.get(id=object_id)
        try:
            inventory_entries = inventory_identifier.objects.all().filter(rel_object = item).order_by('-date')
        except inventory_identifier.DoesNotExist:
            raise Http404
        
        quantity = 0
        for entry in inventory_entries:
            if entry.entree:
                quantity += entry.quantite
            else:
                quantity -= entry.quantite 
            
    except identifier.DoesNotExist:
        raise Http404
    
    return render(request, 'inventory_detail_base.html', {'object_name': object_name,
                                                   'item': item,
                                                   'inventory_entries': inventory_entries,
                                                   'total_quantity': quantity,
                                                   'form': InventoryForm()
                                                   })