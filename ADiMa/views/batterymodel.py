'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *


def list_battery_models(request, list_start=0, list_end=50):
    models_list = ModeleBatterie.objects.all()[int(list_start):int(list_end)]
    return render(request, "list_models_battery.html", {'models_list': models_list,
                                                        'type': "batterie",
                                                        'link_show': "show battery model",
                                                        'link_create': "create battery model",
                                                        })

def show_battery_model(request, product_id):
    if request.method == 'POST':
        model_form = BatteryModelForm(request.POST)
        if model_form.is_valid():
            try:
                model = ModeleBatterie.objects.get(id=request.product_id)
                model.nom = request.POST['nom']
                model.marque_bat = int(request.POST['marque_bat'])
                model.save()
            except ModeleBatterie.DoesNotExist:
                raise Http404
        else:
            return HttpResponse("invalid")
    else:
        try:
            model = ModeleBatterie.objects.get(id=product_id)
        except ModeleBatterie.DoesNotExist:
            return Http404
        
    model_form = BatteryModelForm(initial=model.get_form_init())
    return render(request, 'show_model_battery.html', {'model' : model,
                                                    'model_form' : model_form})

def create_battery_model(request):
    if request.method == 'POST':
        form = BatteryModelForm(request.POST)
        if form.is_valid():
            model = ModeleBatterie()
            model.nom = request.POST['nom']
            try:
                model.marque_bat = MarqueVelo.objects.get(id=int(request.POST['marque_bat']))
            except MarqueVelo.DoesNotExist:
                raise Http404
            model.save()
            return redirect('show battery model', product_id = model.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BikeModelForm()
    
    return render(request, 'show_model_battery_create.html', {'model_form' : form,})