'''
Created on 13 sept. 2014

@author: Jean-Vincent
'''
from django.shortcuts import render, redirect
from django.http.response import Http404
from django.http import HttpResponse
from ADiMa.models import *
from ADiMa.forms import *


def list_bikes(request, list_start=0, list_end=50):
    bikes_list = Velo.objects.all()[int(list_start):int(list_end)]
    return render(request, "list_bikes_base.html", {'bikes_list': bikes_list,})

def show_bike(request, product_id):
    if request.method == 'POST':
        bike_form = BikeForm(request.POST)
        if bike_form.is_valid():
            try:
                bike = Velo.objects.get(id=request.product_id)
            except Velo.DoesNotExist:
                raise Http404
            bike.fill_fields(request.POST)
            bike.save()
            
        else:
            return HttpResponse("invalid")
    else:
        try:
            bike = Velo.objects.get(id=product_id)
        except Velo.DoesNotExist:
            return Http404
        
    bike_form = BikeForm(initial=bike.get_form_init())
    return render(request, 'show_bike.html', {'bike' : bike,
                                              'bike_form': bike_form,
                                              })

def create_bike(request):
    if request.method == 'POST':
        form = BikeForm(request.POST)
        if form.is_valid():
            bike = Velo()
            return redirect('show bike', product_id = bike.id)
        else:
            return HttpResponse("invalid")
    else:
        form = BikeForm()
    
    return render(request, 'show_bike_create.html', {'model_form' : form,})