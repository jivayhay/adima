from django.conf.urls import patterns, include, url
from django.contrib import admin
from ADiMa import views
from ADiMa.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ADiMa.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.default.index, name="index"),
    #
    # CONTACTS
    # list all contacts
    url(r'^contacts$', views.contact.list_contacts, name="list contacts"),
    url(r'^contacts/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.contact.list_contacts, name="list contacts"),
    # show specific contact
    url(r'^contact/(?P<contact_id>\d+)$', views.contact.show_contact, name="show contact"),
    # create contact
    url(r'^contact/create$', views.contact.create_contact, name="create contact"),
    #
    # CLIENTS
    # list all clients
    url(r'^clients$', views.client.list_clients, name="list clients"),
    url(r'^clients/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.client.list_clients, name="list clients"),
    # show specific client
    url(r'^client/(?P<client_id>\d+)$', views.client.show_client, name="show client"),
    # show client by its contact id
    url(r'^client/contact/(?P<contact_id>\d+)$', views.client.show_client_by_contact_id, name="show client contact"),
    #create client
    url(r'^client/create$', views.client.create_client, name="create client"),
    url(r'^client/create/(?P<contact_id>\d+)$', views.client.create_client, name="create client"),
    #
    # RESELLERS
    # list all resellers
    url(r'^resellers$', views.reseller.list_resellers, name="list resellers"),
    url(r'^resellers/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.reseller.list_resellers, name="list resellers"),
    # show specific client
    url(r'^reseller/(?P<reseller_id>\d+)$', views.reseller.show_reseller, name="show reseller"),
    # show client by its contact id
    url(r'^reseller/contact/(?P<contact_id>\d+)$', views.reseller.show_reseller_by_contact_id, name="show reseller contact"),
    #create client
    url(r'^reseller/create$', views.reseller.create_reseller, name="create reseller"),
    url(r'^reseller/create/(?P<contact_id>\d+)$', views.reseller.create_reseller, name="create reseller"),
    #
    # PROSPECTS
    # list all prospects
    url(r'^prospects$', views.prospect.list_prospects, name="list prospects"),
    url(r'^prospects/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.prospect.list_prospects, name="list prospects"),
    # show specific client
    url(r'^prospect/(?P<prospect_id>\d+)$', views.prospect.show_prospect, name="show prospect"),
    # show client by its contact id
    url(r'^prospect/contact/(?P<contact_id>\d+)$', views.prospect.show_prospect_by_contact_id, name="show prospect contact"),
    #create client
    url(r'^prospect/create$', views.prospect.create_prospect, name="create prospect"),
    url(r'^prospect/create/(?P<contact_id>\d+)$', views.prospect.create_prospect, name="create prospect"),
    #
    # BIKES BRANDS
    # list bikes brands
    url(r'^brands/bike/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.bikebrand.list_bike_brands, name="list bike brands"),
    url(r'^brands/bike$', views.bikebrand.list_bike_brands, name="list bike brands"),
    # show bike brand
    url(r'^brand/bike/(?P<brand_id>\d+)$', views.bikebrand.show_bike_brand, name="show bike brand"),
    # create bike brand
    url(r'^brand/bike/create$', views.bikebrand.create_bike_brand, name="create bike brand"),
    #
    # BIKES MODELS
    url(r'^model/bike/$', views.bikemodel.list_bike_models, name="list bike models"),
    url(r'^model/bike/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.bikemodel.list_bike_models, name="list bike models"),
    # show bike model
    url(r'^model/bike/(?P<product_id>\d+)$', views.bikemodel.show_bike_model, name="show bike model"),
    # create bike model
    url(r'^model/bike/create$', views.bikemodel.create_bike_model, name="create bike model"),
    #
    # BIKES VERSIONS
    url(r'^version/bike/$', views.bikeversion.list_bike_versions, name="list bike versions"),
    url(r'^version/bike/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.bikeversion.list_bike_versions, name="list bike versions"),
    # show bike version
    url(r'^version/bike/(?P<product_id>\d+)$', views.bikeversion.show_bike_version, name="show bike version"),
    # create bike version
    url(r'^version/bike/create$', views.bikeversion.create_bike_version, name="create bike version"),
    #
    # BIKES
    url(r'^bike/$', views.bike.list_bikes, name="list bikes"),
    url(r'^bike/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.bike.list_bikes, name="list bikes"),
    # show bike
    url(r'^bike/(?P<product_id>\d+)$', views.bike.show_bike, name="show bike"),
    # create bike
    url(r'^bike/create$', views.bike.create_bike, name="create bike"),
    #
    # BATTERIES BRANDS
    # list batteries brands
    url(r'^brands/battery/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.batterybrand.list_battery_brands, name="list battery brands"),
    url(r'^brands/battery$', views.batterybrand.list_battery_brands, name="list battery brands"),
    # show battery brand
    url(r'^brand/battery/(?P<brand_id>\d+)$', views.batterybrand.show_battery_brand, name="show battery brand"),
    # create battery brand
    url(r'^brand/battery/create$', views.batterybrand.create_battery_brand, name="create battery brand"),
    #
    # BATTERY MODELS
    url(r'^model/battery/$', views.batterymodel.list_battery_models, name="list battery models"),
    url(r'^model/battery/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.batterymodel.list_battery_models, name="list battery models"),
    # show battery model
    url(r'^model/battery/(?P<product_id>\d+)$', views.batterymodel.show_battery_model, name="show battery model"),
    # create battery model
    url(r'^model/battery/create$', views.batterymodel.show_battery_model, name="create battery model"),
    # BATTERY
    # list batteries
    url(r'^battery/$', views.battery.list_batteries, name="list batteries"),
    url(r'^battery/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.battery.list_batteries, name="list batteries"),
    # show battery
    url(r'^battery/(?P<product_id>\d+)$', views.battery.show_battery, name="show battery"),
    # create battery
    url(r'^battery/create$', views.battery.create_battery, name="create battery"),
    #
    # SPARE PARTS BRANDS
    # list spare parts brands
    url(r'^brands/parts/show/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.partsbrand.list_parts_brands, name="list parts brands"),
    url(r'^brands/parts$', views.partsbrand.list_parts_brands, name="list parts brands"),
    # show spare parts brand
    url(r'^brand/parts/(?P<brand_id>\d+)$', views.partsbrand.show_parts_brand, name="show parts brand"),
    # create spare parts brand
    url(r'^brand/parts/create$', views.partsbrand.create_parts_brand, name="create parts brand"),
    #
    # PARTS MODELS
    url(r'^model/parts/$', views.partsmodel.list_parts_models, name="list parts models"),
    url(r'^model/parts/(?P<list_start>\d+)/(?P<list_end>\d+)$', views.partsmodel.list_parts_models, name="list parts models"),
    # show part
    url(r'^model/parts/(?P<product_id>\d+)$', views.partsmodel.show_parts_model, name="show parts model"),
    # create part
    url(r'^model/parts/create$', views.partsmodel.create_parts_model, name="create parts model"),
    #
    # INVENTORY
    url(r'^inventory/(?P<object_name>Velo|PieceDetachee|Batterie)$', views.inventory.list_inventory, name="show inventory"),
    url(r'^inventory/(?P<object_name>Velo|PieceDetachee|Batterie)/(?P<object_id>\d+)$', views.inventory.show_inventory_detail, name="show inventory detail"),
)
